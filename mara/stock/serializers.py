from django.contrib.auth.models import User
from rest_framework import serializers

from .models import *


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = ('id', 'nombre')


class UnidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unidad
        fields = ('id', 'nombre')


class ProductoSerializer(serializers.ModelSerializer):
    categoria_nombre = serializers.ReadOnlyField(source="categoria.nombre")
    unidad_nombre = serializers.ReadOnlyField(source="unidad.nombre")

    class Meta:
        model = Producto
        fields = ('id', 'nombre', 'categoria', 'categoria_nombre', 'codigo', 'unidad', 'unidad_nombre', 'precio_costo', 'precio_venta',
                  'cantidad')


class ProductoVentaSerializer(serializers.ModelSerializer):
    producto = ProductoSerializer(read_only=True)

    class Meta:
        model = ProductoVenta
        fields = ['producto', 'cantidad']


class VentaSerializer(serializers.ModelSerializer):
    productos = ProductoVentaSerializer(many=True, read_only=True)

    class Meta:
        model = Venta
        fields = ('productos', 'fecha', 'razon_social')
        depth = 1
