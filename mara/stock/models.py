from django.db import models

# Create your models here.


class Categoria(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre


class Unidad(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre


class Producto(models.Model):
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    codigo = models.CharField(max_length=100, default='')
    nombre = models.CharField(max_length=200)
    cantidad = models.FloatField(default=0.0)
    unidad = models.ForeignKey(Unidad, on_delete=models.CASCADE)
    precio_costo = models.FloatField(default=0.0)
    precio_venta = models.FloatField(default=0.0)

    def __str__(self):
        return self.nombre


class Venta(models.Model):
    fecha = models.DateTimeField()
    razon_social = models.CharField(default='Anónimo', max_length=100)


class ProductoVenta(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    venta = models.ForeignKey(Venta, on_delete=models.CASCADE, related_name='productos')
    cantidad = models.FloatField(default=0.0)

    def __str__(self):
        return self.producto.nombre
