import django_filters

from .models import Producto


class ProductoFilter(django_filters.FilterSet):
    precio_costo = django_filters.NumberFilter()
    precio_costo__gt = django_filters.NumberFilter(field_name='precio_costo', lookup_expr='gt')
    precio_costo__lt = django_filters.NumberFilter(field_name='precio_costo', lookup_expr='lt')

    precio_venta = django_filters.NumberFilter()
    precio_venta__gt = django_filters.NumberFilter(field_name='precio_venta', lookup_expr='gt')
    precio_venta__lt = django_filters.NumberFilter(field_name='precio_venta', lookup_expr='lt')

    nombre = django_filters.CharFilter(field_name='nombre', lookup_expr='icontains')
    codigo = django_filters.CharFilter(field_name='codigo', lookup_expr='icontains')
    categoria = django_filters.CharFilter(field_name='categoria__nombre', lookup_expr='icontains')
    unidad = django_filters.CharFilter(field_name='unidad__nombre', lookup_expr='exact')

    class Meta:
        model = Producto
        fields = ('precio_costo', 'precio_costo__gt', 'precio_costo__lt', 'precio_venta', 'precio_venta__gt',
                  'precio_venta__lt',
                  'nombre', 'codigo', 'categoria')
