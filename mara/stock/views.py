import django_filters
from django.contrib.auth.models import User

# ViewSets define the view behavior.
from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from tablib import Dataset

from .admin import ProductoResource
from .filters import ProductoFilter
from .models import *
from .serializers import *


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class ProductoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Producto.objects.all().order_by('nombre')
    serializer_class = ProductoSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = ProductoFilter

    @action(detail=False, methods=['post'])
    def importar(self, request, pk=None):
        """importar productos desde un csv."""
        file = request.FILES['file0']
        dataset = Dataset()
        imported_data = dataset.load(file.read().decode("utf-8"))
        producto_resource = ProductoResource()
        result = producto_resource.import_data(dataset, dry_run=True)
        if not result.has_errors():
            producto_resource.import_data(dataset,
                                          dry_run=False)  # Actually import now        return Response({'error': False}, status=status.HTTP_201_CREATED)
            return Response({'error': False}, status=status.HTTP_200_OK)
        else:
            return Response({'error': True}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class CategoriaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer


class UnidadViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Unidad.objects.all()
    serializer_class = UnidadSerializer


class VentaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Venta.objects.all().order_by('fecha')
    serializer_class = VentaSerializer


def index(request, path=''):
    """
    The home page. This renders the container for the single-page app.
    """
    return render(request, 'index.html')
