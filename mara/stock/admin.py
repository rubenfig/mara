from django.contrib import admin

# Register your models here.
from import_export import resources, fields
from import_export.admin import ImportExportModelAdmin
from import_export.widgets import ForeignKeyWidget

from .models import *





class ProductoResource(resources.ModelResource):
    categoria = fields.Field(
        column_name='categoria',
        attribute='categoria',
        widget=ForeignKeyWidget(Categoria))

    unidad = fields.Field(
        column_name='unidad',
        attribute='unidad',
        widget=ForeignKeyWidget(Unidad))

    class Meta:
        model = Producto
        fields = ('id', 'nombre', 'categoria', 'codigo', 'unidad', 'precio_costo', 'precio_venta', 'cantidad')

    def before_import_row(self, row, **kwargs):
        value = row['categoria']
        obj = Categoria.objects.filter(nombre=value)  # create object place
        if not obj.exists():
            obj = Categoria.objects.create(nombre=value)
        else:
            obj = obj.first()
        row['categoria'] = obj.id  # update value to id ob new object

        value = row['unidad']
        obj = Unidad.objects.filter(nombre=value)  # create object place
        if not obj.exists():
            obj = Unidad.objects.create(nombre=value)
        else:
            obj = obj.first()
        row['unidad'] = obj.id  # update value to id ob new object


class ProductoAdmin(ImportExportModelAdmin):
    list_display = ('nombre', 'codigo', 'categoria', 'unidad', 'precio_costo', 'precio_venta', 'cantidad')
    list_filter = ['categoria', 'unidad']
    list_per_page = 10
    search_fields = ['nombre', 'codigo']
    resource_class = ProductoResource


admin.site.register(Producto, ProductoAdmin)
admin.site.register(Categoria)
admin.site.register(Unidad)
admin.site.register(ProductoVenta)
admin.site.register(Venta)
