import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {handleError} from '../util/error-handler';
import {HttpClient} from '@angular/common/http';
import {URL_SERVER} from '../config/constants';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  private url = URL_SERVER + '/categoria/';

  constructor(private http: HttpClient) { }

  listar(params): Observable<any> {

    return this.http.get<any[]>(this.url ,
      {
        params: params
      })
      .pipe(
        catchError(handleError('codigoMensaje', {}))
      );
  }

  obtener(id): Observable<any> {

    return this.http.get<any[]>(this.url + id ,
      {})
      .pipe(
        tap((response: any) => {
          console.log(response.data);
        }),
        catchError(handleError('codigoMensaje', {}))
      );
  }


  modificar(data): Observable<any> {

    return this.http.put<any[]>(this.url + data.id ,
      data)
      .pipe(
        tap((response: any) => {
          console.log(response.data);
        }),
        catchError(handleError('codigoMensaje', {}))
      );
  }


  crear(data): Observable<any> {

    return this.http.post<any[]>(this.url ,
      data)
      .pipe(
        tap((response: any) => {
          console.log(response.data);
        }),
        catchError(handleError('codigoMensaje', {}))
      );
  }





}
