import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {handleError} from '../util/error-handler';
import {HttpClient} from '@angular/common/http';
import {URL_SERVER} from '../config/constants';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = URL_SERVER + '/login';

  constructor(private http: HttpClient) { }

  login(data): Observable<any> {

    return this.http.post<any[]>(this.url ,
      data)
      .pipe(
        tap((response: any) => {
          if (response.token) {
            localStorage.setItem('token', response.token);
          }
        }),
        catchError(handleError('codigoMensaje', {}))
      );
  }



}
