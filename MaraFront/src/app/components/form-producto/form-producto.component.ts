import { Component, OnInit } from '@angular/core';
import {Productoservice} from '../../services/producto.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UnidadService} from '../../services/unidad.service';
import {CategoriaService} from '../../services/categoria.service';
import {AlertModalComponent} from '../alert-modal/alert-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-form-producto',
  templateUrl: './form-producto.component.html',
  styleUrls: ['./form-producto.component.scss']
})
export class FormProductoComponent implements OnInit {
  form: FormGroup;
  id= null;
  categorias = [];
  unidades = [];
  submitted = false;
  constructor(public productoService: Productoservice,
              public unidadService: UnidadService,
              public categoriaService: CategoriaService,
              public formBuilder: FormBuilder,
              public route: ActivatedRoute,
              public router: Router,
              private modalService: NgbModal,
              private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      nombre: ['', Validators.required],
      categoria: [null, Validators.required],
      unidad: [null, Validators.required],
      codigo: [''],
      precio_costo: ['', [Validators.required]],
      precio_venta: ['', Validators.required],
      cantidad: [0, Validators.required]
    });
    this.id = this.route.snapshot.params.id;
    if(this.id){
      this.spinner.show();
      this.productoService.obtener(this.id).subscribe((result) => {
        this.spinner.hide();
        if(!result.error){
          this.form.patchValue(result);
        }
      })
    }
    this.listarCategorias();
    this.listarUnidades();


  }

  listarCategorias(){

    this.categoriaService.listar({}).subscribe((result) => {
      this.categorias = result.results;
    })
  }

  listarUnidades(){

    this.unidadService.listar({}).subscribe((result) => {
      this.unidades = result.results;
    })
  }

  get f(){
    return this.form.controls;
  }

  guardar() {
    if (this.form.valid){
      if (this.id) {
        this.spinner.show();
        this.productoService.modificar(Object.assign({}, this.form.value, {id: this.id})).subscribe((result) => {
          if(!result.error){
            this.spinner.hide();
            this.abrirModalMail();
          }
        })
      }else {
        this.spinner.show();
        this.productoService.crear(this.form.value).subscribe((result) => {
          this.spinner.hide();
          if(!result.error){
            this.abrirModalMail();
          }
        })
      }
    }else{
      this.submitted = true;
    }
  }

  abrirModalMail() {
    const alertModal = this.modalService.open(AlertModalComponent, {ariaLabelledBy: 'modal-alert-mail', windowClass: '', centered: true});
    alertModal.componentInstance.titulo = 'Aviso';
    alertModal.componentInstance.mensaje = 'Se guardó correctamente el producto!';
    alertModal.componentInstance.textoBoton = 'Ok';
    alertModal.result.then((result) => {
      this.router.navigate([''])
    }, (reason) => {
      this.router.navigate([''])
    });
  }

}
