import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-alert-modal',
  templateUrl: './alert-modal.component.html',
  styleUrls: ['./alert-modal.component.scss']
})
export class AlertModalComponent implements OnInit {
  @Input() mensaje: string = '';
  @Input() titulo: string = '';
  @Input() textoBoton: string = '';
  @Input() closeButton: boolean = false;

  constructor(public activeModal: NgbActiveModal) {

  }

  ngOnInit() {
  }

}
