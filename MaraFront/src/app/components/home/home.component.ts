import { Component, OnInit } from '@angular/core';
import {Productoservice} from '../../services/producto.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AlertModalComponent} from '../alert-modal/alert-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {URL_SERVER} from '../../config/constants';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  productos = [];
  total = 0;
  page = 1;
  form: FormGroup;
  mostrarImportar = false;
  afuConfig = {
    multiple: false,
    formatsAllowed: ".csv",
    maxSize: "1",
    uploadAPI:  {
      url: URL_SERVER + '/producto/importar/',
    },
    theme: "dragNDrop",

  };


  constructor(public productoService: Productoservice,
              public formBuilder: FormBuilder,
              public router: Router,
              public modalService: NgbModal,
              private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      nombre: [''],
      categoria: [''],
      unidad: [''],
      codigo: ['']
    });
    this.listar(null);

    this.form.valueChanges.subscribe(val => {
      this.listar(null);
    });

  }

  listar(event){
    let params = {page: event ? event : this.page};
    let formValue = this.form.value;
    for (let key in formValue) {
      if (formValue.hasOwnProperty(key) && formValue[key].length > 0 ) {
        params[key] = formValue[key];
      }
    }
    this.spinner.show();
    this.productoService.listar(params).subscribe((result) => {
      this.spinner.hide();
      this.productos = result.results;
      this.total = result.count;
    })
  }

  editar(producto) {
    this.router.navigate(['producto', {id: producto.id}])
  }


  borrar(producto) {
    this.spinner.show();
    this.productoService.borrar(producto.id).subscribe((result) => {
      this.spinner.hide();
      if(!result || !result.error){
        this.abrirModal();
      }
    })

  }

  abrirModal() {
    const alertModal = this.modalService.open(AlertModalComponent, {ariaLabelledBy: 'modal-alert-mail', windowClass: '', centered: true});
    alertModal.componentInstance.titulo = 'Aviso';
    alertModal.componentInstance.mensaje = 'Se eliminó el producto!';
    alertModal.componentInstance.textoBoton = 'Ok';
    alertModal.result.then(() => {
      this.listar(null);
    },() => {
      this.listar(null);
    })
  }

  DocUpload(event){
    if(event.response && !event.response.error){
      this.listar(null);
    }
  }

}
