import {Component, OnChanges, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{
  mobile: boolean = false;
  loggedIn: boolean = false;
  constructor(public router: Router) {

  }

  ngOnInit() {
    if (window.screen.width <= 576) { // 768px portrait
      this.mobile = true;
    }
    this.loggedIn = !!localStorage.getItem('currentUser');
  }

  goToLogin() {
    this.router.navigate(['login', {}])
  }


}
